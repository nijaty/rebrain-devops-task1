
<img src="https://blog.pregistry.com/wp-content/uploads/2016/10/AdobeStock_113401251-1.jpg" width="400" height="300">


# BMI Calculator on Python

The BMI arithmetic formula is below:

<img src="https://www.bmi-online.org/images/bmi-formula.png" width="400" height="200">

**BMI calculator** can help you to calculate your **Body Mass Index (BMI)**

This program gets from the user some inputs (**height and weight**) and base on these inputs it calculates your **BMI**.

There are **4 BMI Categories** and base on the output result it shows to which category you belongs.
:octocat:

**BMI Categories:**



**1. Underweight = <18.5**\
**2. Normal weight = 18.5–24.9**\
**3. Overweight = 25–29.9**\
**4. Obesity = BMI of 30 or greater**

There are some countries Mean BMI (kg/m²), World Health Organization (WHO), 2015:

|    | Country           | Both | Male | Female |
| -- | ----------------- | ---- | ---- | ------ |
| 20 | 🇺🇸 United States  | 28.5 | 28.5 |  28.5  |
| 37 | 🇦🇿 Azerbaijan     | 27.4 | 26.6 |  28.1  |
| 40 | 🇬🇧 United Kingdom | 27.3 | 27.5 |  27.1  | 
| 41 | 🇬🇷 Greece         | 27.3 | 27.4 |  27.2  | 
| 45 | 🇨🇦 Canada         | 27.2 | 27.6 |  26.8  |
| 48 | 🇬🇪 Georgia        | 27.2 | 27.2 |  27.3  |
| 63 | 🇪🇸 Spain          | 26.7 | 27.4 |  26.0  |
| 72 | 🇷🇺 Russia         | 26.5 | 26.1 |  26.8  | 
| 74 | 🇵🇱 Poland         | 26.4 | 27.0 |  25.7  | 
| 79 | 🇩🇪 Germany        | 26.3 | 27.0 |  25.6  |  

## Acknowledgements

 - [More about BMI](https://en.wikipedia.org/wiki/Body_mass_index)
 - [List of countries by BMI](https://en.wikipedia.org/wiki/List_of_countries_by_body_mass_index)
 
  
### 🚀 About Me
I'm a Core Network Engineer. I'm preparing to become the DevOps Engineer :octocat:.

  
#### Installation

Clone the BMI repository into you local PC

```bash
$ git clone https://github.com/nijaty/bmi_calculator
```
    



##### Run the program

Open the terminal and run the *bmi.py* by the python and input your **height and weight**.
    
```bash
$ python3 bmi.py 
 --- Welcome, to the BMI Calculator --- 
Please, enter your height in m: 1.65
Please, enter your weight in kg: 66
Your bmi is 24 and you are Normal weight

```

John Ridley said:

> "For the vast majority of those who are obese - those with a Body Mass Index over 30 - their size is their choice. They choose to take in more calories than they burn. They choose to take in high fat calories over low-fat ones. They choose to fad diet, if they choose to diet at all."

                                                                                                                                    


  


  
